# Numbers Calculator Client

To start your client:

* Ensure that `Numbers Server` is running
* Install dependencies with `npm install`
* Edit `config.js` to change server hosp, port, API url and commands file name if needed
* Start application with `npm start`, result will be printed to console
* _commands file name can be additionaly provided as `COMMANDS_FILE` environment variable (`COMMANDS_FILE=test_commands.txt npm start`)_

## Additional Information

Any incorrect command will be ignored (and logged) but if Server request will fail for some reason (returning 400 error or 503 timeout) then all client operations will be halted.

Result of test input execution is: `0893456767`
