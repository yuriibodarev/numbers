const config = {
  app: {
    commandsFile: './commands.txt',
  },
  numbers_server: {
    host: 'http://localhost',
    port: 4000,
    api: 'api/calculator',
  },
};

module.exports = config;
