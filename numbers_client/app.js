const fs = require('fs');
const axios = require('axios');
const config = require('./config');

function readFile(path, opts = 'utf8') {
  const p = new Promise((res, rej) => {
    fs.readFile(path, opts, (err, data) => {
      if (err) rej(err);
      res(data);
    });
  });

  return p;
}

async function executeClearCommand(httpClient, res) {
  const url = '/clear';

  const response = await httpClient.post(url);

  if (response.status !== 200) {
    throw Error(`Clear command response error: ${response.status} ${response.data}`);
  }

  return res;
}

async function executeAddCommand(httpClient, number, res) {
  const url = '/add';

  const response = await httpClient.post(url, {
    number,
  });

  if (response.status !== 200) {
    throw Error(`Add ${number} command response error: ${response.status} ${response.data}`);
  }

  return res;
}

async function executeChecksumCommand(httpClient, res) {
  const url = '/checksum';

  const response = await httpClient.get(url);

  if (response.status !== 200) {
    throw Error(`Checksum command response error: ${response.status} ${response.data}`);
  }

  res.push(response.data.result);

  return res;
}

function executeCommand(cmd, httpClient, res) {
  cmd.trim();

  let newRes;

  if (cmd === 'C') {
    newRes = executeClearCommand(httpClient, res);
  } else if (cmd.match(/^A[0-9]+$/)) {
    newRes = executeAddCommand(httpClient, cmd.substring(1), res);
  } else if (cmd === 'CS') {
    newRes = executeChecksumCommand(httpClient, res);
  } else if (cmd !== '') {
    console.warn(`incorrect command: ${cmd}`);
    newRes = Promise.resolve(res);
  } else {
    newRes = Promise.resolve(res);
  }

  return newRes;
}

async function executeCommands() {
  const commandsFile = process.env.COMMANDS_FILE || config.app.commandsFile;

  const {
    numbers_server: {
      host,
      port,
      api,
    },
  } = config;

  const httpClient = axios.create({
    baseURL: `${host}:${port}/${api}`,
    timeout: 5000,
  });

  try {
    const data = await readFile(commandsFile);
    const commands = data.split('\n');

    const checksums = await commands.reduce(async (resPromise, cmd) => {
      const res = await resPromise;

      return executeCommand(cmd, httpClient, res);
    }, Promise.resolve([]));

    console.info(`checksumms: ${checksums}`);
    console.log(`result: ${checksums.join('')}`);
  } catch (err) {
    console.error(err);
  }
}

executeCommands();
