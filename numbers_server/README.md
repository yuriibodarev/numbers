# Numbers Calculator Server

To start your server:

* Install dependencies with `mix deps.get`
* Start Phoenix endpoint with `mix phx.server`
* Test your server with: `mix test`

Now you can interact with your server with `curl`:

**Get checksum**: `curl http://localhost:4000/api/calculator/checksum`
Response example: `{"error":null,"ok":true,"result":0}`

**Add number**: `curl -H "Content-Type: application/json" -d '{"number":"123"}' http://localhost:4000/api/calculator/add`
Response: `{"error":null,"ok":true,"result":null}`
_number parameter must be provided as a STRING containing 0-9 digits, 400 error will be returned in a case of incorrect number parameter_
_Response example:_ `{"error":"invalid_number","ok":false,"result":null}`

**Clear number**: `curl -X POST http://localhost:4000/api/calculator/clear`
Response example: `{"error":null,"ok":true,"result":null}`
_in a case when checksum calculation will take longer than 15ms (config parameter) 503 error will be returned_
_Response example:_ `{"error":"checksum_timeout","ok":false,"result":null}`

## Additional Information

Actually `Numbers.Worker.convert_str_num_to_digits/1` does not need to reverse the final result because `Numbers.Worker.Calculator.add/2` can be modified to accept second argument without reversing it. The first argument comes from `Numbers.Worker` GenServer state and must be reversed anyway. But such modification will make logic and tests unclear so I decided to leave this code 'AS IS'.
