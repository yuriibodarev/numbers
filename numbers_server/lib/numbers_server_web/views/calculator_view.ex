defmodule NumbersWeb.CalculatorView do
  use NumbersWeb, :view

  def render("result.json", %{result: result}) do
    %{ok: true, result: result, error: nil}
  end
end
