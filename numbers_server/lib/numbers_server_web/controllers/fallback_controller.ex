defmodule NumbersWeb.FallbackController do
  use NumbersWeb, :controller

  alias NumbersWeb.ErrorView

  def call(conn, {:error, :invalid_number}) do
    conn
    |> put_status(400)
    |> put_view(ErrorView)
    |> render("400.json", error: :invalid_number)
  end

  def call(conn, {:error, :checksum_timeout}) do
    conn
    |> put_status(503)
    |> put_view(ErrorView)
    |> render("503.json", error: :checksum_timeout)
  end
end
