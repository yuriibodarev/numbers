defmodule NumbersWeb.CalculatorController do
  use NumbersWeb, :controller

  alias Numbers.Worker

  action_fallback NumbersWeb.FallbackController

  def add(conn, %{"number" => number}) when is_binary(number) do
    with :ok <- Worker.add(number), do: render(conn, "result.json", result: nil)
  end

  def add(_, _), do: {:error, :invalid_number}

  @spec clear(Plug.Conn.t(), any()) :: Plug.Conn.t()
  def clear(conn, _) do
    with :ok <- Worker.clear(), do: render(conn, "result.json", result: nil)
  end

  def checksum(conn, _) do
    with {:ok, result} <- Worker.checksum(), do: render(conn, "result.json", result: result)
  end
end
