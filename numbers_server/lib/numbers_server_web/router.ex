defmodule NumbersWeb.Router do
  use NumbersWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", NumbersWeb do
    pipe_through :api

    scope("/calculator") do
      post("/add", CalculatorController, :add)
      post("/clear", CalculatorController, :clear)
      get("/checksum", CalculatorController, :checksum)
    end
  end
end
