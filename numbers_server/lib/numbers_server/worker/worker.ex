defmodule Numbers.Worker do
  use GenServer

  alias Numbers.Worker.Calculator

  require Logger

  defp get_checksum_timeout, do: Application.get_env(:numbers_server, :checksum_timeout, 15)

  # Server (callbacks)

  @impl true
  def init(_) do
    {:ok, {[0], 0}}
  end

  @impl true
  def handle_cast({:add, number}, {current_value, _}) do
    new_value = Calculator.add(current_value, number)

    {:noreply, {new_value, :recalculate}}
  end

  @impl true
  def handle_cast(:clear, _) do
    {:noreply, {[0], 0}}
  end

  @impl true
  def handle_call(:checksum, _from, {current_value, current_checksum}) do
    new_checksum =
      if current_checksum == :recalculate do
        Calculator.checksum(current_value)
      else
        current_checksum
      end

    {:reply, new_checksum, {current_value, new_checksum}}
  end

  # Client

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def add(number_str) when is_binary(number_str) do
    with {:ok, number_digits} <- convert_str_num_to_digits(number_str) do
      GenServer.cast(__MODULE__, {:add, number_digits})
      :ok
    else
      {:error, reason} ->
        Logger.error("numbers worker add call error: #{inspect(reason)}")
        {:error, reason}
    end
  end

  def clear do
    GenServer.cast(__MODULE__, :clear)
    :ok
  end

  def checksum() do
    try do
      res = GenServer.call(__MODULE__, :checksum, get_checksum_timeout())
      {:ok, res}
    catch
      :exit, {:timeout, _error} ->
        Logger.warn("numbers worker checksum call timeout")
        {:error, :checksum_timeout}
    end
  end

  def convert_str_num_to_digits(str_num) when is_binary(str_num) do
    do_convert_str_num_to_digits(String.graphemes(str_num), [])
  end

  defp do_convert_str_num_to_digits([], []), do: {:error, :invalid_number}

  defp do_convert_str_num_to_digits([], result), do: {:ok, Enum.reverse(result)}

  defp do_convert_str_num_to_digits([h | t], result) do
    case Integer.parse(h) do
      {n, ""} -> do_convert_str_num_to_digits(t, [n | result])
      _ -> {:error, :invalid_number}
    end
  end
end
