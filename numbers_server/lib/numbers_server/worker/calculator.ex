defmodule Numbers.Worker.Calculator do
  @moduledoc """
  Calculations
  """

  def add(number1, number2) when is_list(number1) and is_list(number2) do
    do_add(Enum.reverse(number1), Enum.reverse(number2), 0, [])
  end

  def checksum(number) when is_list(number) do
    do_cheksum(number, {0, 0}, true)
  end

  defp do_add([], [], 0, result), do: result

  defp do_add([], [], carry, result), do: [carry | result]

  defp do_add([], number2, carry, result), do: do_add([0], number2, carry, result)

  defp do_add(number1, [], carry, result), do: do_add(number1, [0], carry, result)

  defp do_add([h1 | t1], [h2 | t2], carry, result) do
    case h1 + h2 + carry do
      num when num < 10 -> do_add(t1, t2, 0, [num | result])
      num when num >= 10 -> do_add(t1, t2, div(num, 10), [rem(num, 10) | result])
    end
  end

  defp do_cheksum([], {odd_sum, even_sum}, _) do
    case rem(odd_sum * 3 + even_sum, 10) do
      0 -> 0
      rem -> 10 - rem
    end
  end

  defp do_cheksum([h | t], {odd_sum, even_sum}, is_odd?) do
    res =
      if is_odd? do
        {odd_sum + h, even_sum}
      else
        {odd_sum, even_sum + h}
      end

    do_cheksum(t, res, not is_odd?)
  end
end
