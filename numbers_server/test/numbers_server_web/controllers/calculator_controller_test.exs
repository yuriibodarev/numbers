defmodule NumbersWeb.CalculatorControllerTest do
  use NumbersWeb.ConnCase, async: false

  describe "calculator API can return correct result" do
    setup do
      start_supervised!(Numbers.Worker)
      :ok
    end

    test "for checksum request", %{conn: conn} do
      %{"ok" => true, "result" => 0, "error" => nil} =
        conn
        |> get(Routes.calculator_path(conn, :checksum))
        |> json_response(200)
    end

    test "for addition request", %{conn: conn} do
      %{"ok" => true, "result" => nil, "error" => nil} =
        conn
        |> post(Routes.calculator_path(conn, :add), %{number: "1"})
        |> json_response(200)

      %{"ok" => true, "result" => nil, "error" => nil} =
        conn
        |> post(Routes.calculator_path(conn, :add), %{number: "10"})
        |> json_response(200)

      %{"ok" => true, "result" => 6, "error" => nil} =
        conn
        |> get(Routes.calculator_path(conn, :checksum))
        |> json_response(200)
    end

    test "for clear request", %{conn: conn} do
      %{"ok" => true, "result" => nil, "error" => nil} =
        conn
        |> post(Routes.calculator_path(conn, :add), %{number: "1234567890"})
        |> json_response(200)

      %{"ok" => true, "result" => nil, "error" => nil} =
        conn
        |> post(Routes.calculator_path(conn, :clear))
        |> json_response(200)

      %{"ok" => true, "result" => 0, "error" => nil} =
        conn
        |> get(Routes.calculator_path(conn, :checksum))
        |> json_response(200)
    end

    test "for checksum request timeout", %{conn: conn} do
      %{"ok" => true, "result" => nil, "error" => nil} =
        conn
        |> post(Routes.calculator_path(conn, :add), %{number: "9" <> String.duplicate("0", 1_000_000)})
        |> json_response(200)

      %{"ok" => false, "result" => nil, "error" => "checksum_timeout"} =
        conn
        |> get(Routes.calculator_path(conn, :checksum))
        |> json_response(503)
    end
  end

  describe "calculator can return correct error" do
    test "for invalid add parameters", %{conn: conn} do
      %{"ok" => false, "result" => nil, "error" => "invalid_number"} =
        conn
        |> post(Routes.calculator_path(conn, :add), %{number: ""})
        |> json_response(400)

      %{"ok" => false, "result" => nil, "error" => "invalid_number"} =
        conn
        |> post(Routes.calculator_path(conn, :add), %{number: nil})
        |> json_response(400)

      %{"ok" => false, "result" => nil, "error" => "invalid_number"} =
        conn
        |> post(Routes.calculator_path(conn, :add), %{})
        |> json_response(400)

      %{"ok" => false, "result" => nil, "error" => "invalid_number"} =
        conn
        |> post(Routes.calculator_path(conn, :add), %{number: "123a456"})
        |> json_response(400)
    end
  end
end
