defmodule Numbers.WorkerTest do
  use ExUnit.Case, async: false

  alias Numbers.Worker

  describe "convert_str_num_to_digits can" do
    test "convert correct string number representation into a list of digits" do
      assert {:ok, [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]} == Worker.convert_str_num_to_digits("1234567890")
    end

    test "returns error in a case of incorrect string number representation" do
      assert {:error, :invalid_number} == Worker.convert_str_num_to_digits("")
      assert {:error, :invalid_number} == Worker.convert_str_num_to_digits("one")
      assert {:error, :invalid_number} == Worker.convert_str_num_to_digits("12a45")
    end
  end

  describe "Worker" do
    setup do
      start_supervised!(Numbers.Worker)
      :ok
    end

    test "starts with 0 checksum" do
      assert {:ok, 0} == Worker.checksum()
    end

    test "starts with 0 number" do
      :ok = Worker.add("0")
      assert {:ok, 0} == Worker.checksum()
    end

    test "can add one number" do
      :ok = Worker.add("1")
      assert {:ok, 7} == Worker.checksum()
    end

    test "can add multiple numbers" do
      :ok = Worker.add("1")
      :ok = Worker.add("10")
      :ok = Worker.add("100")
      :ok = Worker.add("1000")
      assert {:ok, 2} == Worker.checksum()
    end

    test "can cl;ear number" do
      :ok = Worker.add("1234567890")
      :ok = Worker.clear()
      assert {:ok, 0} == Worker.checksum()
    end

    test "can return error on checksum timeout" do
      :ok = Worker.add("9" <> String.duplicate("0", 1_000_000))
      assert {:error, :checksum_timeout} == Worker.checksum()
      Process.sleep(500)
      assert {:ok, 3} == Worker.checksum()
    end
  end
end
