defmodule Numbers.Worker.CalculatorTest do
  use ExUnit.Case, async: true

  alias Numbers.Worker.Calculator

  describe "add can" do
    test "add small numbers" do
      assert [0] = Calculator.add([0], [0])
      assert [1] = Calculator.add([0], [1])
      assert [9] = Calculator.add([9], [0])

      assert [1, 0] = Calculator.add([5], [5])
      assert [1, 8] = Calculator.add([9], [9])
    end

    test "add big numbers" do
      assert [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] = Calculator.add([9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9], [1])
      assert [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8] = Calculator.add([9], [9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9])

      assert [7, 4, 9, 5, 6, 1, 8, 8, 9, 0, 7, 9] =
               Calculator.add(
                 Calculator.add([2, 3, 6, 3, 6, 3, 5, 6, 5, 5, 6, 8], [4, 0, 3, 8, 2, 3, 7, 6, 8, 8, 6, 7]),
                 [1, 0, 9, 3, 7, 4, 5, 5, 4, 6, 4, 4]
               )
    end

    test "add colossal numbers" do
      res = List.duplicate(2, 100)
      assert res == Calculator.add(List.duplicate(1, 100), List.duplicate(1, 100))

      res = [1 | List.duplicate(9, 99_999)] ++ [8]
      assert res == Calculator.add(List.duplicate(9, 100_000), List.duplicate(9, 100_000))

      res = [1 | List.duplicate(0, 1_000_000)]
      assert res == Calculator.add(List.duplicate(9, 1_000_000), [1])
    end
  end

  describe "checksum can" do
    test "calculate checksum of small number" do
      assert 0 == Calculator.checksum([0])
      assert 7 == Calculator.checksum([1])
      assert 6 == Calculator.checksum([1, 1])
      assert 3 == Calculator.checksum([1, 1, 1])
      assert 2 == Calculator.checksum([1, 1, 1, 1])
      assert 9 == Calculator.checksum([1, 1, 1, 1, 1])
      assert 0 == Calculator.checksum([1, 2, 1, 2])
    end

    test "calculate checksum of large number" do
      assert 7 == Calculator.checksum([5, 4, 8, 9, 8, 5, 0, 3, 5, 4])
    end

    test "calculate checksum of colossal number" do
      assert 3 == Calculator.checksum([9 | List.duplicate(0, 1_000_000)])
    end
  end
end
